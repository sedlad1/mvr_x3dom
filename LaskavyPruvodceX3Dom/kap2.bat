mkdir kap2

aopt -i ../LaskavyPruvodce/kap2/o-2-1.wrl -x kap2/o-2-1.x3d 
aopt -i ../LaskavyPruvodce/kap2/osy1.wrl -x kap2/osy1.x3d
aopt -i ../LaskavyPruvodce/kap2/p-2-1.wrl -x kap2/p-2-1.x3d
aopt -i ../LaskavyPruvodce/kap2/p-2-2.wrl -x kap2/p-2-2.x3d

aopt -i ../LaskavyPruvodce/kap2/o-2-1.wrl -N kap2/o-2-1.html 
aopt -i ../LaskavyPruvodce/kap2/osy1.wrl -N kap2/osy1.html
aopt -i ../LaskavyPruvodce/kap2/p-2-1.wrl -N kap2/p-2-1.html
aopt -i ../LaskavyPruvodce/kap2/p-2-2.wrl -N kap2/p-2-2.html

rem nasledujici radky jsou potrebuji ke spusteni cygwin - provadi automatickou nahradu retezcu
sed -i 's/.wrl/.x3d/g' kap2/o-2-1.x3d
sed -i 's/.wrl/.x3d/g' kap2/o-2-1.html