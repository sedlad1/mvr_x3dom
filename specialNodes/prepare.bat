mkdir bingeo_is
mkdir bingeo_sacp
mkdir bingeo_splitted
mkdir bingeo_cLhota
mkdir imggeo

cp x3d/statue_low.jpg statue_low.jpg

aopt -i x3d/statue_low.x3d -g "imggeo/:s" -x statue_low_imggeo.x3d
aopt -i statue_low_imggeo.x3d -N statue_low_imggeo.html

aopt -i x3d/statue_low.x3d -G "bingeo_is/:is" -x statue_low_is.x3d
aopt -i statue_low_is.x3d -N statue_low_is.html

aopt -i x3d/statue_low.x3d -G "bingeo_sacp/:sacp" -x statue_low_sacp.x3d
aopt -i statue_low_sacp.x3d -N statue_low_sacp.html

aopt -i wrl/from_123D_low.wrl -F "IndexedFaceSet:opt(1),maxtris(20000)" -x tmp.x3d
aopt -i tmp.x3d -G "bingeo_splitted/:sacp" -x statue_splitted.x3d
aopt -i statue_splitted.x3d -N statue_splitted.html

cp wrl/from_123D_low_tex_0.jpg from_123D_low_tex_0.jpg
rm tmp.x3d

aopt -i wrl/cLhota.wrl -F "IndexedFaceSet:opt(1),maxtris(50000)" -x tmp.x3d
aopt -i tmp.x3d -G "bingeo_cLhota/:sacp" -x cLhota.x3d
aopt -i cLhota.x3d -N cLhota.html
cp wrl/cLhota*.jpg ./
rm tmp.x3d 