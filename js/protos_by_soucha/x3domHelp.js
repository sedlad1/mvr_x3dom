/**
 * @author Michal Soucha
 */

/**
 * @param {string} inName
 * @param {string} id
 * @returns {Element} element with id 'inName__id'
 */
function getInlinedElement(inName, id) {
    return document.getElementById(inName + '__' + id);
}

/**
 * Create colorChaser element
 * @param {string} id
 * @param {string} duration
 * @param {string} initDest
 * @param {string} initVal
 * @returns {Element} colorChaser
 */
function colorChaser(id, duration, initDest, initVal) {
    return chaser('color', id, duration, initDest, initVal);
}

/**
 * Create positionChaser element
 * @param {string} id
 * @param {string} duration
 * @param {string} initDest
 * @param {string} initVal
 * @returns {Element} positionChaser
 */
function positionChaser(id, duration, initDest, initVal) {
    return chaser('position', id, duration, initDest, initVal);
}

/**
 * Create orientationChaser element
 * @param {string} id
 * @param {string} duration
 * @param {string} initDest
 * @param {string} initVal
 * @returns {Element} orientationChaser
 */
function orientationChaser(id, duration, initDest, initVal) {
    return chaser('orientation', id, duration, initDest, initVal);
}

/**
 * Create 'type'Chaser element
 * @param {string} type
 * @param {string} id
 * @param {string} dur
 * @param {string} initDest
 * @param {string} initVal
 * @returns {Element} chaser
 */
function chaser(type, id, dur, initDest, initVal) {
    var ch = document.createElement(type + 'Chaser');
    ch.setAttribute('id', id);
    ch.setAttribute('duration', dur);
    ch.setAttribute('initialDestination', initDest);
    ch.setAttribute('initialValue', initVal);
    return ch;
}

/**
 * Create ROUTE element.
 * @param {string} fromNode
 * @param {string} fromField
 * @param {string} toNode
 * @param {string} toField
 * @returns {Element} ROUTE element
 */
function route(fromNode, fromField, toNode, toField) {
    var r = document.createElement('ROUTE');
    r.setAttribute('fromNode', fromNode);
    r.setAttribute('fromField', fromField);
    r.setAttribute('toNode', toNode);
    r.setAttribute('toField', toField);
    return r;
}