/** 
 * This script tries to load prototypes.
 * 
 * Setup scene:
 * 1. call function <b>load()</b> inside <b>document.onload()</b>
 * 2. implement sceneLoad() function which will be called after loading prototypes
 * 
 * If the scene were loaded and you add a prototype instance to the scene,
 * you can call <b>processProto(scene)</b> or better <b>loadProto(parentOfInstance, protoDeclare)</b>
 * when you had obtained ProtoDeclare element.
 * 
 * Background of loading process:
 * I. Inline elements
 *   1. load() finds all Inline element in x3dom scene and marks them as rootInline
 *      (note that these element are not yet loaded)
 *   2. each rootInline gets onload listener in which there is control whether
 *      exists some Inline element inside currently loaded Inline
 *      (Inline element is loaded asynchronously to x3dom)
 *   3. when the entire rootInline is loaded (determined by counter of inner Inlines)
 *      every child of rootInline is cloned into rootInline's parent with removal
 *      inner Inlines such way that children of inner Inline are tranfered
 *      to parent of inner Inline and inner Inline element is deleted
 *   4. after loading and editing all rootInlines is called inlinedSceneLoaded()
 *      that invokes user's sceneLoaded() after processProtos(scene)
 * II. extern prototypes - first phase of processProtos() -> loadExternProto(ExternProtoDeclare)
 *   1. loadExternProto() as the first gain related ProtoDeclare via synchronous XMLHttpRequest
 *   2. in loaded ProtoDeclare there is changed name according to ExternProtoDeclare
 *      and also all relative urls (inside ExternProtoDeclares and Inlines)
 *   3. ExternProtoDeclare is replaced by adjusted ProtoDeclare
 * III. prototypes - second phase of processProtos() -> loadProto(scene, ProtoDeclare)
 *   1. for each instance of the prototype is first made deep copy of ProtoInterface's fields
 *      that is initialized according to attributes of instance
 *   2. the entire content of ProtoBody is packeged in a new Group element with some changes:
 *      a. IS and Connect elements are removed and bound fields are set
 *          by updated interface values
 *      b. Group id attribute is set to the string created as: name and number of the prototype instance
 *      c. each attribute: DEF, USE and fromNode, toNode (ROUTE element) are updated
 *          to inlined version with namespace set to Group's id
 *          example: DEF='ok' inside prototype 'cube0' => DEF='cube0__ok'
 *      d. where a DEF attribute is set, there is also the same value assigned to the attribute id
 * 
 * @author Michal Soucha (based on David Sedlacek's script)
 */

var rootInlineInScene = 0;

/**
 * It is called after the entire scene with possibly Inline elements loaded.<br>
 * It only call processProto() with Scene element and the user's sceneLoaded().
 */
function inlinedSceneLoaded() {
    var scene = document.getElementsByTagName('scene')[0];
    if (!scene) {
        console.error('X3D Scene not found!');
    }
    processProtos(scene);

    // whole scene is loaded
    sceneLoaded();
}

/**
 * Interpret prototypes after loading Inline elements.<br>
 * <br>
 * It must be called only once, and must be placed in <b>document.onload()</b>.
 */
function load() {
    var scene = document.getElementsByTagName('scene')[0];
    if (!scene) {
        console.error('X3D Scene not found!');
    }
    var inlineIns = scene.getElementsByTagName('inline');
    rootInlineInScene = inlineIns.length;
    for (var i = 0; i < inlineIns.length; i++) {
        initInlineParent(inlineIns[i]);
    }
    if (rootInlineInScene === 0) {
        inlinedSceneLoaded();
    }
}

/**
 * Initialize rootInline, which means add listener and counter of inner Inlines.
 * @param {Element} ins rootInline
 */
function initInlineParent(ins) {
    var parent = new InlineNode(ins);
    //console.log(parent);
    ins.addEventListener('load', function(ev) {
        inlineLoaded(parent, ev.target);
    });
}

/**
 * @constructor
 * @param {Element} ins Inline
 */
function InlineNode(ins) {
    this.elem = ins;
    this.count = 1;
}

/**
 * Handler of Inline onload event.
 * @param {InlineNode} parentInline reference to rootInline
 * @param {Element} act Inline which was actually loaded
 */
function inlineLoaded(parentInline, act) {
    var ins = act.getElementsByTagName('Inline');
    parentInline.count += ins.length - 1;
    //console.log(parentInline.count, act.getAttribute('load'), act);
    if (parentInline.count === 0) {
        transformInline(parentInline.elem);
    }
    for (var i = 0; i < ins.length; i++) {
        //console.log(i, ins[i], ins[i].getAttribute('load'));
        
        // inner Inline was processed by x3dom
        if (ins[i].getAttribute('load') === 'true' || ins[i].hasAttribute('USE')) {
            ins[i].addEventListener('load', function(ev) {
                inlineLoaded(parentInline, ev.target);
            });
        } else {// inner Inline is placed inside loaded prototype
            parentInline.count--;
        }
    }
    if (parentInline.count === 0) {
        transformInline(parentInline.elem);
    }
}

/**
 * Move children of given rootInline to parent of rootInline<br>
 * with removal of inner Inline elements.
 * @param {Element} inlineIns loaded rootInline
 */
function transformInline(inlineIns) {
    var el;
    for (var i = 0; i < inlineIns.childNodes.length; i++) {
        el = inlineIns.childNodes[i].cloneNode(true);
        var ins = el.getElementsByTagName('Inline');
        //console.log(el,ins.length);
        for (var j = 0; j < ins.length; j++) {
            // was inner Inline process by x3dom?
            if (ins[j].getAttribute('load') === 'true') {
                while (ins[j].hasChildNodes()) {
                    ins[j].parentNode.appendChild(ins[j].removeChild(ins[j].firstChild));//.cloneNode(true)
                }
                ins[j].parentNode.removeChild(ins[j]);
                j--;
            } 
        }
        inlineIns.parentNode.appendChild(el);
    }
    //console.log(inlineIns, rootInlineInScene);

    // removes children from inline by x3dom
    inlineIns.setAttribute('url', '');
    
    // are all rootInlines loaded?
    if (--rootInlineInScene === 0) {
        inlinedSceneLoaded();
    }
}

/**
 * Loads ExternProtoDeclare and interprets prototypes using founded ProtoDeclare.
 * @param {Element} x3dNode 
 */
function processProtos(x3dNode) {
    var protos = x3dNode.getElementsByTagName('ExternProtoDeclare');
    if (protos.length === 0) {
        protos = x3dNode.getElementsByTagName('externprotodeclare');
    }
    console.log(protos.length, 'Externproto declare found');
    for (var i = 0; i < protos.length; i++) {
        var proto = loadExternProto(protos[i]);
        if (proto) {
            //console.log('new extern proto',i,proto);
            protos[i].parentNode.appendChild(proto);
            protos[i].parentNode.removeChild(protos[i]);
            i--;
        }
    }
    protos = x3dNode.getElementsByTagName('ProtoDeclare');
    if (protos.length === 0) {
        protos = x3dNode.getElementsByTagName('protodeclare');
    }
    console.log(protos.length, 'proto declare found');
    for (var i = 0; i < protos.length; i++) {
        loadProto(x3dNode, protos[i]);
    }
    //console.log(x3dNode.nodeName, x3dNode.childNodes.length);
}

/**
 * Tries to load ProtoDeclare corresponding to given ExternProtoDeclare
 * @param {Element} externproto ExternProtoDeclare
 * @returns {Element|null} founded ProtoDeclare or null
 */
function loadExternProto(externproto) {
    var protoUrl = externproto.getAttribute('url');
    protoUrl = protoUrl.replace(/"/g, '');
    var idx = protoUrl.indexOf("#");
    var name = protoUrl.substr(idx + 1);
    protoUrl = protoUrl.substr(0, idx);
    console.log('loading', protoUrl, name);
    var p = null;
    var scene = loadX3DScene(protoUrl);
    //console.log('loaded', scene);
    if (scene !== undefined && scene) {
        var protos = scene.getElementsByTagName('ProtoDeclare');
        if (protos.length === 0) {
            protos = scene.getElementsByTagName('protodeclare');
        }
        for (var i = 0; i < protos.length; i++) {
            if (protos[i].getAttribute('name') === name) {
                p = protos[i].cloneNode(true);
                // update name attribute accordint to externProto value
                p.setAttribute('name', externproto.getAttribute('name'));
                updateExternProtoUrl(p, protoUrl);
                break;
            }
        }
    }
    //console.log(p);
    return p;
}

/**
 * Updates relative address inside given prototype with given base url.<br>
 * Urls are updated in inner ExternProtoDeclare and Inline elements.
 * @param {Element} proto ProtoDeclare
 * @param {String} url in format baseUrl/someSuffix
 */
function updateExternProtoUrl(proto, url) {
    var idx = url.lastIndexOf('/');
    if (idx === -1)
        return;
    var baseUrl = url.substr(0, idx);
    var protos = proto.getElementsByTagName('ExternProtoDeclare');
    if (protos.length === 0) {
        protos = proto.getElementsByTagName('externprotodeclare');
    }
    for (var i = 0; i < protos.length; i++) {
        var protoUrl = protos[i].getAttribute('url'), base = baseUrl;
        protoUrl = protoUrl.replace(/"/g, '');
        //console.log(protoUrl);
        protoUrl = base.concat(((protoUrl.charAt(0) === '/') ? '' : '/'), protoUrl);
        console.log('Updated externproto url', protoUrl);
        protos[i].setAttribute('url', protoUrl);
    }
    var inl = proto.getElementsByTagName('inline');
    if (inl.length === 0) {
        inl = proto.getElementsByTagName('Inline');
    }
    for (var i = 0; i < inl.length; i++) {
        var inlUrl = inl[i].getAttribute('url'), base = baseUrl;
        inlUrl = inlUrl.replace(/"/g, '');
        //console.log(inlUrl);
        inlUrl = base.concat(((inlUrl.charAt(0) === '/') ? '' : '/'), inlUrl);
        console.log('Updated inline url', inlUrl);
        inl[i].setAttribute('url', inlUrl);
    }
}

/**
 * Interpret all founded instances of given prototype.
 * @param {Element} x3dNode Scene or some html element which contains instance of prototype
 * @param {Element} proto ProtoDeclare
 */
function loadProto(x3dNode, proto) {
    var protoI = getProtoInterface(proto);

    // find instances
    var protoInstances = x3dNode.getElementsByTagName(protoI.name);
    var insCount = 0;
    if (proto.hasAttribute('instances')) {
        insCount = parseInt(proto.getAttribute('instances'));
    }
    console.log('protoI.name: ' + protoI.name, protoInstances.length + ' new instances', insCount + ' older');

    for (var j = 0; j < protoInstances.length; j++) {
        //console.log(j, protoInstances[j]);
        var g = document.createElement('Group');

        // instaces with USE attribute should not have other attributes and can be interpret as empty Group
        if (protoInstances[j].hasAttribute("USE")) {
            g.setAttribute("USE", protoInstances[j].getAttribute("USE"));
            protoInstances[j].parentNode.appendChild(g);
            continue;
        } else if (protoInstances[j].hasAttribute("use")) {
            g.setAttribute("USE", protoInstances[j].getAttribute("use"));
            protoInstances[j].parentNode.appendChild(g);
            continue;
        }
        
        var protoVal = prepareRealProtoValues(protoI, protoInstances[j]);
        if ('DEF' in protoVal) {
            g.setAttribute("DEF", protoVal['DEF']);
        }
        if ('id' in protoVal) {
            g.setAttribute("id", protoVal['id']);
        } else {// set unique id value
            g.setAttribute("id", protoI.name + (j + insCount));
        }
        
        var protoBody = proto.getElementsByTagName('ProtoBody')[0];
        //console.log(j, 'body',protoBody);

        // clones interpreted protoBody to instance
        for (var k = 0; k < protoBody.childNodes.length; k++) {
            // safe check - only element should be copied
            if (protoBody.childNodes[k].nodeType === Node.ELEMENT_NODE) {
                //console.log(k, protoBody.childNodes[k]);
                var element = protoBody.childNodes[k].cloneNode(true);

                replaceProtoValues(element, protoVal);

                setNamespace(protoI.name + (j + insCount), element, true);
                g.appendChild(element);
            }
        }
        protoInstances[j].parentNode.appendChild(g);
    }
    
    // update instances count
    proto.setAttribute('instances', insCount + protoInstances.length);
    //* // remove all prototype instance element because they will be interpreted again with loadProto call
    while (protoInstances.length > 0) {
        protoInstances[0].parentNode.removeChild(protoInstances[0]);
    }//*/
}

/**
 * Prepares Object containing prototype name and fields of ProtoInterface.
 * @param {Element} proto ProtoDeclare
 * @returns {Object}
 */
function getProtoInterface(proto) {
    var prI = {}; //protoInterface object

    prI.name = proto.getAttribute('name');

    var protoI = proto.getElementsByTagName('ProtoInterface');
    var protoIfields = protoI[0].getElementsByTagName('field');

    for (var j = 0; j < protoIfields.length; j++) {
        // beware of storing and accessing element with lower case
        var attrName = protoIfields[j].getAttribute('name').toLowerCase();
        prI[attrName] = {};
        prI[attrName].value = protoIfields[j].getAttribute('value');
        prI[attrName].accessType = protoIfields[j].getAttribute('accessType');
    }
    //console.log(prI);
    return prI;
}

/**
 * Makes copy of given protoInterface and updates values according to attributes in given instance.<br>
 * <br>
 * <b>Need jQuery<b>
 * @param {Object} protoInterface object obtained from getProtoInterface()
 * @param {Element} instance element with tag corresponding to protoInterface
 * @returns {Object} updated protoInterface
 */
function prepareRealProtoValues(protoInterface, instance) {
    var prI = jQuery.extend(true, {}, protoInterface);
    
    //console.log(prI);
    for (var i = 0; i < instance.attributes.length; i++) {
        // some attributes does not in protoInterface so add them
        if (instance.attributes[i].name.toUpperCase() === "DEF") {
            prI['DEF'] = instance.attributes[i].value;
        } else if (instance.attributes[i].name.toLowerCase() === "id") {
            prI['id'] = instance.attributes[i].value;
        } else {
            prI[instance.attributes[i].name.toLowerCase()].value = instance.attributes[i].value;
        }
    }
    return prI;
}

/**
 * Sets attributes to elements that correspond to found IS element, the values obtained from protoInterface.<br>
 * And then removes IS and connect elements.
 * @param {Element} element 
 * @param {Object} protoInterface updated version obtained from prepareRealProtoValues()
 */
function replaceProtoValues(element, protoInterface) {

    var isElem = element.getElementsByTagName('IS');
    if (isElem.length === 0) {
        isElem = element.getElementsByTagName('is');
    }

    //for (var j = 0; j < isElem.length; j++) {
    while (isElem.length > 0) {// isElems are gradually removed
        var isElement = isElem[0];
        var isParent = isElement.parentNode;

        var connections = isElement.getElementsByTagName('connect');
        for (var k = 0; k < connections.length; k++) {
            // beware of storing and accessing element with lower case
            var fieldName = connections[k].getAttribute('protoField').toLowerCase();
            if (protoInterface.hasOwnProperty(fieldName)) { // safe check - exists fieldName in interface?
                isParent.setAttribute(connections[k].getAttribute('nodeField'), protoInterface[fieldName].value);
            }
        }
        isParent.removeChild(isElem[0]);
    }

}

if (!Array.forEach) {
    Array.forEach = function(array, fun, thisp) {
        var len = array.length;
        for (var i = 0; i < len; i++) {
            if (i in array) {
                fun.call(thisp, array[i], i, array);
            }
        }
    };
}

/**
 * Recursive function for updating attributes id, DEF, USE and fromNode, toNode (ROUTE element).
 * @param {String} prefix namespace
 * @param {Element} childDomNode root element where updating starts
 * @param {boolean} mapDEFToID if only DEF is set and mapDEFToID is true then id is set with same value as DEF
 */
function setNamespace(prefix, childDomNode, mapDEFToID) {
    if (childDomNode instanceof Element && childDomNode.setAttribute !== undefined) {
        if (childDomNode.hasAttribute('id')) {
            childDomNode.setAttribute('id', prefix.toString().replace(' ', '') + '__' + childDomNode.getAttribute('id'));
        }
        if (childDomNode.hasAttribute('DEF')) {
            childDomNode.setAttribute('DEF', prefix.toString().replace(' ', '') + '__' + childDomNode.getAttribute('DEF'));
            if (mapDEFToID) {
                childDomNode.setAttribute('id', childDomNode.getAttribute('DEF'));
            }
        } else if (childDomNode.hasAttribute('USE')) {
            childDomNode.setAttribute('USE', prefix.toString().replace(' ', '') + '__' + childDomNode.getAttribute('USE'));
        }
        if (childDomNode.localName.toUpperCase() === 'ROUTE') {
            childDomNode.setAttribute('fromNode', prefix.toString().replace(' ', '') + '__' + childDomNode.getAttribute('fromNode'));
            childDomNode.setAttribute('toNode', prefix.toString().replace(' ', '') + '__' + childDomNode.getAttribute('toNode'));
        }
    }
    if (childDomNode.hasChildNodes()) {
        Array.forEach(childDomNode.childNodes, function(children) {
            setNamespace(prefix, children, mapDEFToID);
        });
    }
}

/**
 * Standart synchronous XMLHttpRequest.
 * @param {String} dname url of loaded X3D file
 * @returns {Element|null} Scene element
 */
function loadX3DScene(dname) {
    //var xhttp = new window.XMLHttpRequest();
    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    } else {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    /*
     xmlhttp.onreadystatechange = function() {
     if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
     
     }
     };
     */
    if (xhttp.overrideMimeType) {
        xhttp.overrideMimeType('text/xml');
    }
    xhttp.open("GET", dname, false);
    xhttp.send();
    var inlScene = null, xml = null;
    if (navigator.appName !== "Microsoft Internet Explorer") {
        xml = xhttp.responseXML;
    }

    if (xml === null) {
        xml = new DOMParser().parseFromString(xhttp.responseText, "text/xml");
    }
    //console.log(xml,xhttp.responseXML,xhttp.responseText);
    if (xml !== undefined && xml !== null) {
        inlScene = xml.getElementsByTagName('Scene')[0] || xml.getElementsByTagName('scene')[0];
    }
    return inlScene;
}

