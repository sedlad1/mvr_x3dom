function processProtos(x3dNode) {
    //console.log('x3dNode: '+x3dNode);
    protos = x3dNode.getElementsByTagName('protodeclare');
    if(protos.length == 0)
        protos = x3dNode.getElementsByTagName('ProtoDeclare');
    //console.log('protos: '+protos);
    //console.log(protos);
    for(var i=0; i < protos.length; i++) {
        proto = protos[i];

        var protoI = getProtoInterface(proto);
        //console.log('protoI.name: '+protoI.name);

        //najdu v dokumentu reference na objekt a zacne nahrazovani
        protoInstances = x3dNode.getElementsByTagName(protoI.name);
        for(var j = 0; j < protoInstances.length; j++) {
            //console.log(j+"/"+protoInstances.length+" "+protoInstances[j]);

            var protoVal = prepareRealProtoValues(protoI, protoInstances[j]);
            
            var protoBody = proto.getElementsByTagName('ProtoBody')[0];
            //console.log(protoBody.childNodes[1]);

            for(k = 0; k < protoBody.childNodes.length; k++) {
                //testuji zda mam doopravdy X3Dom node
                if (protoBody.childNodes[k].nodeType === Node.ELEMENT_NODE) {
                  //console.log(k+" "+protoBody.childNodes[k]);
                  var element = protoBody.childNodes[k].cloneNode(true);

                  replaceProtoValues(element, protoVal);
                  //replaceProtoValues(element, protoI);

                  protoInstances[j].parentNode.appendChild(element);
                }
            }
        }
    }
}

//precte zahlavi PROTOtypu a pripravi si jsObject pro snadnejsi praci
function getProtoInterface(proto) {
    var prI = {}; //protoInterface object

    prI.name = proto.getAttribute('name');

    protoI = proto.getElementsByTagName('ProtoInterface');
    protoIfields = protoI[0].getElementsByTagName('field');

    for(var j = 0; j < protoIfields.length; j++) {
        //POZOR - toLowerCase je tu proto, ze u instanci jsou z nejakeho neznameho duvodu jmena atributu malym pismem - podobne v replaceProtoValues()
        var attrName = protoIfields[j].getAttribute("name").toLowerCase();
        prI[attrName] = {};
        prI[attrName].value = protoIfields[j].getAttribute("value");
        prI[attrName].accessType = protoIfields[j].getAttribute("accessType");
    }
    //console.log(prI);

    return prI;
}

//udela kopii zahlavi prototypu a zmeny atributy dle pozadavku instance
function prepareRealProtoValues(protoInterface, instance) {
    //console.log(protoInterface);
    var prI = {};
    
    for (var key in protoInterface) {
        if (protoInterface.hasOwnProperty(key)) {
            if(typeof protoInterface[key] != "object")
                prI[key] = protoInterface[key];
            else {
                prI[key] = {};
                for (var keyInner in protoInterface[key]) {
                    prI[key][keyInner] = protoInterface[key][keyInner];
                }
            }
        }
    }
    
    //console.log(prI);
    //console.log(instance);
    for(var i = 0; i < instance.attributes.length; i++) {
        //POZOR - instance.attributes[i].name je z nejakeho neznameho duvodu malymi pismeny, proto jsou v nekterych funkcich attr.toLowerCase()...
        prI[instance.attributes[i].name.toLowerCase()].value = instance.attributes[i].value;
    }

    return prI;
}

//udela kopii zahlavi prototypu a zmeny atributy dle pozadavku instance
// -- old-jQuery  version
function prepareRealProtoValues_old(protoInterface, instance) {
    //console.log(protoInterface);
    
    var prI = jQuery.extend(true, {}, protoInterface); //hluboka kopie objektu - vznikla mi tu zavislost na jquery !
    //var prI = JSON.decode(JSON.encode(protoInterface)); //hluboka kopie objektu - asi by se melo resit jinak !
        
    //console.log(prI);

    for(var i = 0; i < instance.attributes.length; i++) {
        //POZOR - instance.attributes[i].name je z nejakeho neznameho duvodu malymi pismeny, proto jsou v nekterych funkcich attr.toLowerCase()...
        prI[instance.attributes[i].name].value = instance.attributes[i].value;
    }

    return prI;
}

//nahradi v elementu atributy definovane pomoci IS a CONNECT ze ty v jsObjektu zahlaviPrototypu
function replaceProtoValues(element, protoInterface) {

    isElem = element.getElementsByTagName('is');
    if(isElem.length == 0) {
        isElem = element.getElementsByTagName('IS');
    }

    for(var j = 0; j < isElem.length; j++) {
        isParent = isElem[j].parentNode;

        connections = isElem[j].getElementsByTagName('connect');
        for(var k = 0; k < connections.length; k++) {    
            //POZOR - toLowerCase je tu proto, ze u instanci jsou z nejakeho neznameho duvodu jmena atributu malym pismem - podobne v getProtoInterface()
            var fieldName = connections[k].getAttribute('protoField').toLowerCase(); 
            if(protoInterface.hasOwnProperty(fieldName)) { //pro jistotu si overime, ze ta vlastnost existuje v zahlavi
                isParent.setAttribute(connections[k].getAttribute('nodeField'), protoInterface[fieldName].value);
            }
        }
    }
}


